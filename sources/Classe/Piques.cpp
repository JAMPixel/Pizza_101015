#include "Piques.hpp"

Piques::Piques(){
	mPosX = 0;
	mPosY = 0;
	mHeight = 16;
	mWidth = 32;
	mImage = "../../ressources/images/piques.png";
}
Piques::Piques(float posX ,float posY,std::string image, int i){
	mPosX = posX;
	mPosY = posY;
	mHeight = 16;
	mWidth = 32;
	mImage = image;
	id=i;
}
