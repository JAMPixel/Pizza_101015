#include "Bloc.hpp"


	float Bloc::getX() const{
		return mPosX;
	}
	
	float Bloc::getY() const{
		return mPosY;
	}
	
	int Bloc::getHeight() const {
		return mHeight;
	}
	
	int Bloc::getWidth () const{
		return mWidth;
	}
	
	std::string Bloc::getImage() const{
		return mImage;
	}
	Bloc::Bloc (float posX , float posY,std::string image, int i){
		mPosX = posX;
		mPosY = posY;
		mHeight = 32;
		mWidth = 32;
		mImage = image;
		id=i;
	}
	Bloc :: Bloc(){
		mPosX = 0;
		mPosY = 0;
		mHeight = 32;
		mWidth = 32;
		mImage = "../../ressources/images/bloc.png";
	}
	
	
	

