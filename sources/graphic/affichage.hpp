#ifndef AFFICHAGE_H
#define AFFICHAGE_H
#include "../Classe/Map.hpp"
#include "../Classe/Bloc.hpp"
#include "../Classe/Piques.hpp"
#include "../Classe/Porte.hpp"
#include "../Classe/Perso.hpp"
#include <SDL2/SDL.h>
#include <iostream>
#include "SDL2/SDL_image.h"
#include <vector>
extern SDL_Renderer* render ;
extern SDL_Rect coorRender ;
extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH ;
extern const int TAILLE;

void refresh(int largeur,int hauteur,Map &map);
void affichePerso(Perso perso);



#endif
