#include <iostream>
#include  <fstream>
#include "Map.hpp"

Map::Map() {

}

Map::Map(std::string s) {
	int i=0, type=-1;
	int ligne=0,colonne=0;
	
	std::string nom=s;
	std::ifstream fichier(nom.c_str());
	
	if(fichier)
	{
		fichier >> largeur_;
		fichier >> hauteur_;
		
		tab_.resize(largeur_*hauteur_,0);

		while(!fichier.eof() && i<largeur_*hauteur_)
		{
			if(((i)*32)%(largeur_*32)==0 && i!=0){
					ligne ++;
					colonne = 0;
			}
		 	fichier >> type;
		  	switch(type){
		  		case 1 :
		  			tab_[i]=new Bloc(colonne*32.0f,ligne*32.0f,"../ressources/images/bloc1.png",1);
		  			colonne++;
		  			break;
		  		
		  		case 2 :
		  			tab_[i]= new Porte(colonne*32.0f,ligne*32.0f,"../ressources/images/porteSortie.png","../ressources/images/porteSortieOuverte.png",2);
		  			colonne++;
		  			break;
		  			
		  		case 3 :
		  			tab_[i]= new Piques(colonne*32.0f,ligne*32.0f,"../ressources/images/grospics.png",3);
		  			colonne++;
		  			break;
		  			
		  		default :
		  			colonne++;
		  			break;
		  		
		  	}
		  	
			i++;
		}
		fichier.close();
	}
	else
	{
		std::cout << "Erreur ouverture"<< std::endl;
	}
}

std::vector<Bloc*> & Map::getTab_() {
	return tab_;
}

Map::~Map()
{
	for(int i=0;i<(largeur_*hauteur_);i++)
	{
		if(tab_[i]!=0)
		{
			delete tab_[i];
		}
	}
}
