#pragma once
#include "../Classe/Bloc.hpp"
#include "../Classe/Map.hpp"
#include "../Classe/Perso.hpp"
#include "../Classe/Piques.hpp"
#include "../Classe/Porte.hpp"


bool collision(Perso&, Bloc*);
bool collision(Bloc*, Perso&);
bool collision(Perso&, Perso&);
bool collision(Piques&, Perso&);
bool collision(Perso&, Piques&);

bool interaction(Porte&,Perso&);
bool interaction(Porte&,Perso&);
