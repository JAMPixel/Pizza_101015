#ifndef BLOC_H
#define BLOC_H
#include <string>
class Bloc{
	
	public :
		float mPosX;
		float mPosY;
		int mHeight;
		int mWidth;
		std::string mImage;
		int id;
		
		
	public :
		float getX() const;
		float getY() const;
		int getHeight() const;
		int getWidth() const;
		std::string getImage() const;
		Bloc();
		Bloc(float posX, float posY,std::string image, int);
		
};

#endif
