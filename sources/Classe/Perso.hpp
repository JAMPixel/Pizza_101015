#pragma once
#include <string>
#include "Bloc.hpp"

class Map;

class Perso{
	public:
		float x_;
		float y_;
		bool teteHaute_;
		// Indice correspondant au bloc dans map.tab_
		int blocSousPerso;
		std::string img_;
		
	public:
		Perso();
		Perso(Map &);
		float getX_();
		float getY_();
		bool getTeteHaute_();
		void setX_(float);
		void setY_(float);
		void setBlocSousPerso(Map &);
		void inverse(Map &);
		void avancer(bool,Map&,bool);
		void creationPorte(Map&);
		~Perso();
};
