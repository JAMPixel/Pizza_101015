#include "Porte.hpp"


Porte::Porte(){
	mPosX = 0;
	mPosY = 0;
	mHeight = 32;
	mWidth = 24;
	mImage = "../../ressources/images/porte.png";
	mImageOuvert ="../../ressources/image/porteOuverte.png";
	mOuvert = false;
}
Porte::Porte(float posX, float posY,std::string image,std::string imageOuvert, int i){
	mPosX = posX;
	mPosY = posY;
	mHeight = 32;
	mWidth = 24;
	mImage = image;
	mImageOuvert = imageOuvert;
	mOuvert = false;
	id=i;
}

void Porte::setOuvert(bool ouvert){
	if(ouvert!=mOuvert){
		mOuvert = ouvert;
		std::string temp;
		temp = mImageOuvert;
		mImageOuvert = mImage;
		mImage =temp;
	}
		
	
}
