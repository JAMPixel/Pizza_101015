#include "graphic/affichage.hpp"
#include <string>
int main(int argc, char *argv[]){
	
	SDL_Event event;
	std::vector<Bloc> blocDecors;
	std::vector<Piques> piques;
	Perso perso;
	Map map = Map ("../ressources/level/level"+std::string(argv[1]));
	bool continuer = true;
	SDL_Window* window = NULL;
	std::vector<int> bloc;
	SCREEN_WIDTH = map.largeur_*TAILLE;
	SCREEN_HEIGHT = map.hauteur_*TAILLE;
	coorRender = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
	
	if(SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "probleme initialisation SDL : " << SDL_GetError() << std::endl;
		SDL_Quit();
		return EXIT_FAILURE;
	}
	window = SDL_CreateWindow("PizzaJam",0,0,SCREEN_WIDTH,SCREEN_HEIGHT,SDL_WINDOW_SHOWN);
	SDL_SetWindowPosition(window,SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED);
	if(window == NULL){
		std::cout << "probleme initialisation fenetre : "<<SDL_GetError() << std::endl;
		SDL_Quit();
		return -1;
	}
	render = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
	
	if(!render){
		std::cout << "probleme initialisation render : "<< SDL_GetError() << std::endl;
		SDL_DestroyWindow(window);
		SDL_Quit();
		return EXIT_FAILURE;
	}
	
	perso = Perso(map);
	refresh(map.largeur_,map.hauteur_,map);
	affichePerso(perso);
	while(continuer){
		SDL_Delay(10);
		while (SDL_PollEvent(&event)){
			if(event.window.event == SDL_WINDOWEVENT_CLOSE){
				continuer = false;
			}
			if(event.type == SDL_KEYDOWN){
				if(SDL_GetKeyboardState(NULL)[SDL_SCANCODE_UP] && SDL_GetKeyboardState(NULL)[SDL_SCANCODE_LEFT]){
						perso.avancer(false,map,true);
						refresh(map.largeur_,map.hauteur_,map);
						affichePerso(perso);
						do{
							perso.avancer(false,map,false);
							refresh(map.largeur_,map.hauteur_,map);
							affichePerso(perso);
						}while(map.tab_[perso.blocSousPerso]==0);
				}else if(SDL_GetKeyboardState(NULL)[SDL_SCANCODE_UP] && SDL_GetKeyboardState(NULL)[SDL_SCANCODE_RIGHT]){
						perso.avancer(true,map,true);
						refresh(map.largeur_,map.hauteur_,map);
						affichePerso(perso);
						do{
							perso.avancer(true,map,false);
							refresh(map.largeur_,map.hauteur_,map);
							affichePerso(perso);
						}while(map.tab_[perso.blocSousPerso]==0);
					
				}else if(SDL_GetKeyboardState(NULL)[SDL_SCANCODE_RIGHT]){ 
						do{
							perso.avancer(true,map,false);
							refresh(map.largeur_,map.hauteur_,map);
							affichePerso(perso);
						}while(map.tab_[perso.blocSousPerso]==0);
						
					}else if(SDL_GetKeyboardState(NULL)[SDL_SCANCODE_LEFT]){
						do{
							perso.avancer(false,map,false);
							refresh(map.largeur_,map.hauteur_,map);
							affichePerso(perso);
						}while(map.tab_[perso.blocSousPerso]==0);
						
					}else if (SDL_GetKeyboardState(NULL)[SDL_SCANCODE_UP]){
						
						if(map.tab_[perso.blocSousPerso-map.largeur_] != 0 &&map.tab_[perso.blocSousPerso-map.largeur_]->id==2){
							if(static_cast<Porte*>(map.getTab_()[perso.blocSousPerso-map.largeur_])->mOuvert){
								std::cout << "fin niveau" << std::endl;
								SDL_DestroyWindow(window);
								SDL_Quit();
								return EXIT_SUCCESS;
								
							}
						
							static_cast<Porte*>(map.getTab_()[perso.blocSousPerso-map.largeur_])->setOuvert(true);
							refresh(map.largeur_,map.hauteur_,map);
							affichePerso(perso);
						}
						
							
						}
						
					
		
			}
			
		}
	}
	
	SDL_DestroyWindow(window);
	SDL_Quit();
	return EXIT_SUCCESS;
}
