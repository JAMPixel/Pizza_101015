#include <iostream>
#include "Perso.hpp"
#include "Map.hpp"
#include "Porte.hpp"
#include "../physic/Physics.hpp"

int xParcourue=0;
int yParcourue=0;

Perso::Perso(){
	x_=0;
	y_=0;
	teteHaute_=true;
	img_="../ressources/images/StickCourseD1.png";
}

Perso::Perso(Map& m ){

	int i=0;
	
	while (m.tab_[i]!=0)
	{
		i+=m.largeur_;
	}

	x_=0;
	y_=(i)*32/m.largeur_ ;
	
	teteHaute_=true;
	img_="../ressources/images/StickRepos.png";
	
	blocSousPerso=i+m.largeur_;
}


float Perso::getX_() {
	return x_;
}

float Perso::getY_() {
	return y_;
}

bool Perso::getTeteHaute_() {
	return teteHaute_;
}

void Perso::setX_(float x) {
	x_=x;
}

void Perso::setY_(float y) {
	y_=y;
}

void Perso::setBlocSousPerso(Map & m)
{
	blocSousPerso=(y_/32 +1)*m.largeur_+(x_/32);
}


//-----------------------------------------------
void Perso::avancer(bool b, Map & m, bool saut) {
	int blocCoteDroit=blocSousPerso-m.largeur_+1;
	int blocCoteGauche=blocSousPerso-m.largeur_-1;
	bool truc=false;
	if(getTeteHaute_())
	{
		if(m.tab_[blocSousPerso]==0)
		{
			
				y_+=20;
				setBlocSousPerso(m);
				blocCoteDroit=blocSousPerso-m.largeur_+1;
				blocCoteGauche=blocSousPerso-m.largeur_-1;
			
		}
		else
		{
			if(m.tab_[blocSousPerso]->id==3)
			{
				y_+=20;
				setBlocSousPerso(m);
				blocCoteDroit=blocSousPerso-m.largeur_+1;
				blocCoteGauche=blocSousPerso-m.largeur_-1;
				img_="../ressources/images/explosion2.png";
			}
		}
		if(saut) 
		{
			y_-=64;
			setBlocSousPerso(m);
				std::cout << "Bloc sous "<<blocSousPerso << std::endl;
				blocCoteDroit=blocSousPerso-m.largeur_+1;
				if(x_<=32) blocCoteGauche=blocSousPerso-m.largeur_+1;
				else blocCoteGauche=blocSousPerso-m.largeur_-1;
				std::cout << "Bloc droit " << blocCoteDroit << std::endl;
				std::cout << "Bloc gauche " << blocCoteGauche <<" type "<<truc<< std::endl;
		}
		if(!b)
		{
			
			if(x_<=32) blocCoteGauche=blocSousPerso-m.largeur_+1;
			
			if((m.tab_[blocCoteGauche]==0 || typeid(m.tab_[blocCoteGauche])==typeid(Porte)) && x_>0)
			{
				truc=m.tab_[blocCoteGauche]==0;
				x_-=10;
				
				setBlocSousPerso(m);
				std::cout << "Bloc sous "<<blocSousPerso << std::endl;
				blocCoteDroit=blocSousPerso-m.largeur_+1;
				if(x_<=32) blocCoteGauche=blocSousPerso-m.largeur_+1;
				else blocCoteGauche=blocSousPerso-m.largeur_-1;
				std::cout << "Bloc droit " << blocCoteDroit << std::endl;
				std::cout << "Bloc gauche " << blocCoteGauche <<" type "<<truc<< std::endl;
			}
		}
		
		if(b)
		{
			if((m.tab_[blocCoteDroit]==0 || m.tab_[blocCoteDroit]->id==2))
			{
					
					x_+=10;
					
					setBlocSousPerso(m);
					std::cout << "Bloc sous "<<blocSousPerso << std::endl;
					blocCoteDroit=blocSousPerso-m.largeur_+1;
					if(x_<=32) blocCoteGauche=blocSousPerso-m.largeur_;
					else blocCoteGauche=blocSousPerso-m.largeur_-1;
					std::cout << "Bloc droit " << blocCoteDroit << std::endl;
					std::cout << "Bloc gauche " << blocCoteGauche << std::endl;
			}
		}
	}
	else
	{
		
	}
}

//---------------------------------------------

void Perso::inverse(Map& m) {
	int i=blocSousPerso;
	teteHaute_=!teteHaute_;
	if(teteHaute_) img_="../../ressources/images/StickCoursePlafondD1.png";
	else img_="../../ressources/images/StickCourseD1.png";
	
	y_-=8;
	while(m.tab_[i]->mPosX==0)
	{
		i--;
	}
	blocSousPerso=i;
	while(y_>(m.tab_[blocSousPerso]->mPosY+m.tab_[blocSousPerso]->mHeight))
	{
		y_-=5;
	}
}



//----------------------------------------------

void Perso::creationPorte(Map & m) {
	int numcase;
	m.tab_[blocSousPerso-m.largeur_]=new Porte(x_,y_+8,"../ressources/images/porte.png","../ressources/images/porteOuverte.png",2);
	// Case perso : y max / 32
	// Soustraire largeur pour remonter dans le tableau jusqu'à
	// la premiere case de bloc au dessus du perso
	// Rajouter 32 -> on a y_porte
	numcase=blocSousPerso-m.largeur_;
	while(m.tab_[numcase]==0)
	{
		numcase-=m.largeur_;
	}
	
	m.tab_[numcase+m.largeur_]= new Porte (x_,numcase*32,"../ressources/images/porte_inverse.png","../ressources/images/porteOuverte.png",2);
}


Perso::~Perso()
{

}
