#pragma once
#include <string>
#include <vector>
#include "Bloc.hpp"
#include "Porte.hpp"
#include "Piques.hpp"

class Map {
	public:
	int largeur_;
	int hauteur_;
	std::vector<Bloc*> tab_;
	
	public:
		Map();
		Map(std::string);
		std::vector<Bloc*> & getTab_();
		~Map();
};
