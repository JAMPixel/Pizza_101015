#ifndef PORTE_H
#define PORTE_H
#include "Bloc.hpp"
class Porte :public Bloc{
	public :
		Porte();
		Porte(float posX ,float posY,std::string image,std::string imageOuvert, int);
		void setOuvert(bool);
	public :
		bool mOuvert ;
		std::string mImageOuvert;
};




#endif
